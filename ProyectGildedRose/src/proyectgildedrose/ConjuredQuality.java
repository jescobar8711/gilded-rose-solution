/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectgildedrose;

/**
 *
 * @author jescobar
 */
public class ConjuredQuality implements UpdateQuality{

    @Override
    public void quality(Item item) {
     item.setQuality(item.getQuality() - this.incrementarQuality(item));
    }
    
  private int incrementarQuality(Item item) {
          return item.getQuality()- 2 > 0 ? 2 : item.getQuality();
        }
    
}
