/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectgildedrose;

/**
 *
 * @author jescobar
 */
public class DefectoQuality implements UpdateQuality{

    @Override
    public void quality(Item item) {
    item.setQuality(this.incrementarQuality(item));
     }
    
    
   private int incrementarQuality(Item item) {
            if (item.getQuality() < 50) {
               return item.getQuality() - 1;
            }
            return item.getQuality();
        }
    
}
