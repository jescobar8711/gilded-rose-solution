/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectgildedrose;

/**
 *
 * @author jescobar
 */
public class Defecto implements UpdateStrategy{

private UpdateQuality updateQuality;
private UpdateSell updateSell;

    public Defecto() {
        this.updateQuality=new DefectoQuality();
        this.updateSell=new DefectoSell();
    }

    public Defecto(UpdateQuality updateQuality, UpdateSell updateSell) {
        this.updateQuality = updateQuality;
        this.updateSell = updateSell;
    }

    public UpdateQuality getUpdateQuality() {
        return updateQuality;
    }

    public void setUpdateQuality(UpdateQuality updateQuality) {
        this.updateQuality = updateQuality;
    }

    public UpdateSell getUpdateSell() {
        return updateSell;
    }

    public void setUpdateSell(UpdateSell updateSell) {
        this.updateSell = updateSell;
    }
    
    @Override
    public void actualizar(Item item) {
          this.updateQuality.quality(item);
       this.updateSell.Sell(item);
    }
    
}
