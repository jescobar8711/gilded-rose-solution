/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectgildedrose;

/**
 *
 * @author jescobar
 */
public interface UpdateStrategy {

    public void actualizar(Item item);

}
