/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyectgildedrose;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jescobar
 */
public class GildedRose {
    UpdateStrategy updateStrategyDefecto;
       private final Item[] items;
    private static final Map<String, UpdateStrategy> itemStrategy;
	static {
		itemStrategy = new HashMap<String, UpdateStrategy>();
		itemStrategy.put("Aged Brie", new AgedBre());
                itemStrategy.put("Backstage passes to a TAFKAL80ETC concert", new BackStage ());
                 itemStrategy.put("Sulfuras, Hand of Ragnaros", new Sulfuras ());
                 itemStrategy.put("Conjured Mana Cake", new Conjured ());
}
        
  

    public GildedRose(Item[] items) {
        this.items = items;
        this.updateStrategyDefecto=new Defecto();
    }

    public void dactualizar (){
        for (Item item : items) {
            if(!itemStrategy.keySet().contains(item.getName())) {
                this.updateStrategyDefecto.actualizar(item);
            } else {
             itemStrategy.get(item.getName()).actualizar(item);
            }
            
        }
    };
}
